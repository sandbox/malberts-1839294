<?php
/**
 * @file
 *
 * Query string default contextual filter plugin.
 */

/**
 * Implements hook_views_plugins().
 */
function views_query_string_views_plugins() {
  $plugins = array(
    'argument default' => array(
      'views_query_string_url' => array(
        'title' => t('Query string (GET)'),
        'handler' => 'views_query_string_plugin_argument_default_views_query_string_url',
      ),
    )
  );
  return $plugins;
}
