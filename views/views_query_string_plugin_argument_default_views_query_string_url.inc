
<?php
/**
 * @file
 * Contains the Query string contextual filter plugin.
 */

/**
 * Default argument plugin to get a contextual filter value from a query string.
 */
class views_query_string_plugin_argument_default_views_query_string_url extends views_plugin_argument_default {

  function option_definition() {
    $options = parent::option_definition();
    $options['query_string'] = array('default' => '');
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    $form['query_string'] = array(
      '#type' => 'textfield',
      '#title' => t('Query string variable name'),
      '#default_value' => $this->options['query_string'],
      '#description' => t('Enter the query string parameter to get the value from.'),
    );
  }
  
  function get_argument() {
    if (isset($_GET[$this->options['query_string']])) {
      return $_GET[$this->options['query_string']];
    }

    return FALSE;
  }

}
